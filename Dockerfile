FROM ubuntu:16.04 AS builder

RUN apt-get update && apt-get install -y \
        wget git-core \
        sudo diffstat \
        iproute2 iputils-ping \
        python3 python \ 
        gcc make gawk cpio g++ \
        language-pack-en-base locales \
        xz-utils bzip2

# Configure default user
COPY sudoers.user /etc/
RUN groupadd -g 70 user && \
    useradd -N -m -u 70 -g 70 user && \
    echo "user ALL=NOPASSWD:ALL" >> /etc/sudoers

USER user
# Install Poky eSDK toolchain
ADD http://downloads.yoctoproject.org/releases/yocto/yocto-2.4.4/toolchain/x86_64/poky-glibc-x86_64-core-image-minimal-cortexa8hf-neon-toolchain-ext-2.4.4.sh /tmp/sdk_installer.sh

RUN sudo chmod 755 /tmp/sdk_installer.sh
RUN /tmp/sdk_installer.sh -y -d ~/poky_sdk/

# Cleanup SDK installer
RUN sudo rm /tmp/sdk_installer.sh

ENV LANG=en_US.UTF-8

# Extend toolchain with libpng and ffmpeg
RUN echo "LICENSE_FLAGS_WHITELIST=\"commercial\"" >> ~/poky_sdk/conf/local.conf
RUN . ~/poky_sdk/environment-setup-cortexa8hf-neon-poky-linux-gnueabi; devtool sdk-install -s libpng ffmpeg

# Cleanup temp data - WARNING - SDK might not be able to build extra components properly afterwards
WORKDIR /home/user/poky_sdk
RUN rm -rf sstate-cache downloads tmp/work tmp/log tmp/stamps tmp/cache tmp/buildstats

FROM ubuntu:16.04

RUN apt-get update && apt-get install -y \
        openjdk-8-jdk \
        wget git-core \
        sudo diffstat \
        python3 \
        python \
        make swig

ENV LANG=en_US.UTF-8

# Add ipk-build script
COPY ipk-build /usr/bin/ipk-build
RUN chmod a+x /usr/bin/ipk-build

# Configure default user
COPY sudoers.user /etc/
RUN groupadd -g 70 user && \
    useradd -N -m -u 70 -g 70 user && \
    echo "user ALL=NOPASSWD:ALL" >> /etc/sudoers

USER user
# Copy SDK
COPY --from=builder /home/user/poky_sdk /home/user/poky_sdk

